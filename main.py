import argparse
import json
import pprint

from app_logging import Logger

log = Logger(level=0)

parser = argparse.ArgumentParser()

# Required params
parser.add_argument("user", type=str, help="User name")
parser.add_argument("password", type=str, help="User password")

# Optional params
parser.add_argument("-l", "--letters", dest="letters", type=str, help="Letters to filter keys")
parser.add_argument("-f", "--logfile", dest="logfile", action="store_true", help="Export data to logfile")
parser.add_argument("-j", "--journal", dest="journal", action="store_true", help="Logging to journal")

args = parser.parse_args()

# init journal
if (args.journal):
    log.set_logging_file(filename="journal")

data = None

# import json
try:
    with open("./github_api_respond.json") as file:
        data = json.load(file)

except FileNotFoundError as error:
    log.critical(error)

# filter data
filtered_data = {}

if args.letters is not None and data is not None:
    for key in data.keys():
        if key[0].lower() in args.letters:
            filtered_data[key] = data[key]

# log filter data
try:
    if args.logfile:
        with open("./logs/filter.log", "w", encoding="utf8") as file:
            json.dump(filtered_data, file, indent=3)

except FileNotFoundError as error:
    log.critical(error)

# collected data
try:
    if data is not None and data["login"] == args.user and data["password"] == args.password:
        with open("./collected.json", "w", encoding="utf8") as file:
            json.dump(filtered_data, file, indent=3)

except KeyError as error:
    log.error("No such key in imported data: '{}'".format(error))
